import * as React from "react";
import * as ReactDOM from "react-dom";
import {Router, Route, browserHistory} from 'react-router'
import * as b from "react-bootstrap"


class TopTitleBar extends React.Component<{}, {}> {

    doCoolThings () {
    }

    render() {
        // const wellStyles = {maxWidth: 400, margin: '0 auto 10px'};

        return (
            <div className="well">
                <b.Button bsStyle="primary" bsSize="large" block>Block level button</b.Button>
                <b.Button bsSize="large" block>Block level button</b.Button>
            </div>
        )
    }

}

ReactDOM.render(
    (<Router history={browserHistory}>
        <Route path="/" component={TopTitleBar} />
    </Router>),
    document.getElementById("app")
);